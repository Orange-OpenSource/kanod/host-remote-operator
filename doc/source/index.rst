====================
host-remote-operator
====================

Overview
========

Host-remote-operator implements a version of Host resource that synchronizes
with a `real` Host resource on a remote cluster. We will call the first one 
the source Host and the second the target Host.

Architecture
============

The source Host is controlled by a standard controller for Host dedicated to
the kind. It synchronizes with a server on the remote cluster. The credentials
used to establish the dialog are created with a custom resource RemoteNamespace.

As its name implies, RemoteNamespace defines the configuration for creating
Hosts in a given namespace. Reconciliating such a resource will create a secret
that can be used by a source Host to create a synchronized target Host in that
namespace. The secret contains a URL, certificate and credential so that it
can be used from any remote location.

The RemoteNamespace controller implements both the generation of secret and
the synchronization service. It must be exposed as a public facing service.

Usage
=====

The kind associated to this Host variant is ``remote``.

Two label selectors are used to control which target Host is created and synced.

* ``host.kanod.io/real-kind`` is the kind of the target Host.
* ``host.kanod.io/remote-config`` is the name of a secret in the source Host
  namespace containing the endpoint location and the credentials for accessing
  the cluster holding the target Host.

Authentication
==============

To create the configuration secret, a RemoteNamespace object must be created:

.. code-block: yaml

    apiVersion: kanod.io/v1alpha1
    kind: RemoteNamespace
    metadata:
        name: config
        namespace: remote1
    spec:
        URL: https://192.168.133.11/remote
        cluster: cluster1
        secret:
            namespace: capm3
            name: remote-config

The URL identifies the remote endpoint.

.. warning::

    The URL should rather be a parameter of the controller as the end user
    has little control over it.

The secret field specifies where the configuration secret is created. It may
be created in another namespace.

.. warning::

    As the controller for remote Host now allow sources and targets in the same
    namespace, this possibility may disapear in future release for security 
    reasons.

If a cluster field is defined, the credential will be owned by a cluster object
of that name in the same namespace. The main purpose of that field is to
simplify pivoting.

Synchronization contract
========================

* The lifespan of the target is directly linked with the lifespan of
  the source. It should be created as soon as the source is created
  (this may be prohibited by quota webhook) and it must be deleted when
  the source is deleted.
* The specification of the source is copied on the target except for the kind
  and arguments specific to remote Host resources as explained above.
* The status of the target is copied on the source.
* The labels and annotations are copied from the target to the source if they
  do not belong to the ``kubernetes.io`` domains or the ``host.kanod.io``
  domains.
* Annotations are used to keep the list of metadata that have been synchronized.
  deletion of a synchronized metadata on the target will trigger its deletion on
  the source. Other metadata on the source MUST be kept intact.

