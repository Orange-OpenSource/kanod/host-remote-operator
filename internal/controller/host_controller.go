/*
Copyright 2024 Orange.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controller

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"net/http"
	"strconv"
	"strings"
	"time"

	logr "github.com/go-logr/logr"
	hostv1alpha1 "gitlab.com/Orange-OpenSource/kanod/host-operator/api/v1alpha1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
	"sigs.k8s.io/controller-runtime/pkg/event"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	logger "sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

const (
	RealKindLabel       = "host.kanod.io/real-kind"
	RemoteConfigLabel   = "host.kanod.io/remote-config"
	RemoteKeyAnnotation = "host.kanod.io/remote-key"
	RemoteDomain        = "remote.host.kanod.io"
	requeueAfter        = time.Second * 30
)

// HostReconciler reconciles a Host object
type HostReconciler struct {
	client.Client
	Scheme        *runtime.Scheme
	RemoteConfigs map[string]RemoteClient
	channel       chan event.GenericEvent
}

//+kubebuilder:rbac:groups=kanod.io,resources=hosts,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=kanod.io,resources=hosts/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=kanod.io,resources=hosts/finalizers,verbs=update

// Reconcile is part of the main kubernetes reconciliation loop which aims to
// move the current state of the cluster closer to the desired state.
// TODO(user): Modify the Reconcile function to compare the state specified by
// the Host object against the actual cluster state, and then
// perform operations to make the cluster state reflect the state specified by
// the user.
//
// For more details, check Reconcile and its Result here:
// - https://pkg.go.dev/sigs.k8s.io/controller-runtime@v0.15.0/pkg/reconcile
func (r *HostReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	log := logger.FromContext(ctx).WithName("host-controller").WithValues("host", req.NamespacedName)
	host := &hostv1alpha1.Host{}
	if err := r.Client.Get(ctx, req.NamespacedName, host); err != nil {
		if errors.IsNotFound(err) {
			return ctrl.Result{}, nil
		}
		return ctrl.Result{}, err
	}

	if host.Spec.Kind != "remote" {
		// Not handled by this controller.
		log.Info("Skipping not associated to baremetal. Bad Label association")
		return ctrl.Result{}, nil
	}

	if !host.ObjectMeta.DeletionTimestamp.IsZero() {
		return r.reconcileDelete(ctx, log, host)
	}

	if host.Annotations != nil {
		if _, ok := host.Annotations[hostv1alpha1.PausedAnnotation]; ok {
			return ctrl.Result{Requeue: true, RequeueAfter: requeueAfter}, nil
		}
	}

	if !controllerutil.ContainsFinalizer(host, hostv1alpha1.HostFinalizer) {
		controllerutil.AddFinalizer(host, hostv1alpha1.HostFinalizer)
		if err := r.Update(ctx, host); err != nil {
			return ctrl.Result{}, err
		}
	}

	labels := host.Spec.HostSelector.MatchLabels
	if labels == nil {
		err := fmt.Errorf("missing labels")
		log.Error(err, "no labels found")
		return ctrl.Result{}, err
	}
	realKind, ok1 := labels[RealKindLabel]
	remoteConfigName, ok2 := labels[RemoteConfigLabel]
	if !ok1 || !ok2 {
		err := fmt.Errorf("missing labels")
		log.Error(err, "no labels found")
		return ctrl.Result{}, err
	}
	rclient, err := r.getRemoteClient(ctx, log, remoteConfigName, host.Namespace)
	if err != nil {
		log.Error(err, "cannot retrieve remote client")
		return ctrl.Result{}, err
	}
	remoteHostName := fmt.Sprintf("remote-%s", host.Name)
	remoteHost, err := rclient.Get(remoteHostName)
	if err != nil {
		return ctrl.Result{}, err
	}
	if err := r.synchronizeSecret(ctx, log, rclient, host, "user", host.Spec.UserData); err != nil {
		return ctrl.Result{}, err
	}
	if err := r.synchronizeSecret(ctx, log, rclient, host, "network", host.Spec.NetworkData); err != nil {
		return ctrl.Result{}, err
	}
	if err := r.synchronizeSecret(ctx, log, rclient, host, "meta", host.Spec.MetaData); err != nil {
		return ctrl.Result{}, err
	}

	if host.Annotations == nil {
		host.Annotations = map[string]string{}
	}
	copyHost := host.DeepCopy()
	copyHost.Spec.Kind = realKind
	matchLabels := copyHost.Spec.HostSelector.MatchLabels
	matchLabels[hostv1alpha1.HostKindLabel] = realKind
	delete(matchLabels, RealKindLabel)
	delete(matchLabels, RemoteConfigLabel)

	if remoteHost == nil {
		copyHost.Annotations[RemoteKeyAnnotation] = host.Namespace + "/" + host.Name
		rclient.Create(remoteHostName, copyHost)
	} else {
		host.Annotations[hostv1alpha1.SyncedLabelsAnnotation] = syncMaps(
			host.Labels, remoteHost.Labels,
			host.Annotations[hostv1alpha1.SyncedLabelsAnnotation])
		host.Annotations[hostv1alpha1.SyncedAnnotationsAnnotation] = syncMaps(
			host.Annotations, remoteHost.Annotations,
			host.Annotations[hostv1alpha1.SyncedAnnotationsAnnotation])
		copyHost.Annotations[RemoteKeyAnnotation] = host.Namespace + "/" + host.Name
		rclient.Update(remoteHostName, copyHost)

		log.V(1).Info("Synchronized host", "host", host)
		err = r.Update(ctx, host)
		if err != nil {
			log.Error(err, "cannot update metadata of host")
			return ctrl.Result{}, err
		}

		host.Status = remoteHost.Status
		err = r.Status().Update(ctx, host)
		log.V(1).Info("Synchronized host status", "host", host)
		if err != nil {
			log.Error(err, "cannot update status/annotations of host")
			return ctrl.Result{}, err
		}
	}
	return ctrl.Result{}, nil
}

// Get a remote client to connect to a remote cluster
//
// TODO: The life-cycle of this object is not clear. As it is just a secret, we
// cannot really destroy it which is clearly unfortunate. We may end up adding a
// custom resource just to solve this issue and be able to cancel the associated
// watch.
func (r *HostReconciler) getRemoteClient(ctx context.Context, log logr.Logger, name, namespace string) (*RemoteClient, error) {
	keyAnnotConfig := name + "/" + namespace
	log = log.WithValues("configKey", keyAnnotConfig)
	rclient, ok := r.RemoteConfigs[keyAnnotConfig]
	if !ok {
		var remoteConfig corev1.Secret
		keyConfig := types.NamespacedName{Name: name, Namespace: namespace}
		err := r.Get(ctx, keyConfig, &remoteConfig)
		if err != nil {
			log.Error(err, "cannot access remote config secret")
			return nil, err
		}
		rclient.Namespace = string(remoteConfig.Data[NamespaceKey])
		rclient.Password = string(remoteConfig.Data[PasswordKey])
		rclient.URL = string(remoteConfig.Data[UrlKey])
		ca, ok := remoteConfig.Data[CaKey]
		if ok {
			log.V(1).Info("specific transport used.")
			certpool := x509.NewCertPool()
			if !certpool.AppendCertsFromPEM(ca) {
				log.Info("no certificate added")
			}
			rclient.Client = http.Client{
				Transport: &http.Transport{
					TLSClientConfig: &tls.Config{
						RootCAs: certpool,
					},
				},
			}
		} else {
			log.V(1).Info("Default transport.")
			rclient.Client = http.Client{}
		}
		go func() {
			for {
				if err := rclient.Watch(r.channel); err != nil {
					log.Error(err, "watch loop failed retrying")
				}
				time.Sleep(500 * time.Millisecond)
			}
		}()
		r.RemoteConfigs[keyAnnotConfig] = rclient
	}
	return &rclient, nil
}

func (r *HostReconciler) reconcileDelete(ctx context.Context, log logr.Logger, host *hostv1alpha1.Host) (ctrl.Result, error) {
	if controllerutil.ContainsFinalizer(host, hostv1alpha1.HostFinalizer) {
		stillExists := false
		err := func() error {
			if host.Annotations != nil {
				if _, ok := host.Annotations[hostv1alpha1.PausedAnnotation]; ok {
					log.V(1).Info("delete: resource is paused - delete immediately")
					return nil
				}
			}

			labels := host.Spec.HostSelector.MatchLabels
			if labels == nil {
				log.V(1).Info("delete: nothing to perform. No match labels")
				return nil
			}
			remoteConfigName, ok := labels[RemoteConfigLabel]
			if !ok {
				log.V(1).Info("delete: nothing to perform. No label to remote config")
				return nil
			}
			rclient, err := r.getRemoteClient(ctx, log, remoteConfigName, host.Namespace)
			if err != nil && !errors.IsNotFound(err) {
				return err
			}

			remoteHostName := fmt.Sprintf("remote-%s", host.Name)
			remoteHost, err := rclient.Get(remoteHostName)
			if err != nil {
				log.Error(err, "check of remote host existence failed")
				return err
			}

			if remoteHost == nil {
				log.V(1).Info("remote host delete: nothing to perform. remote host missing")
				return nil
			}

			if err := r.deleteSecret(log, rclient, remoteHost, "user", remoteHost.Spec.UserData); err != nil {
				log.Error(err, "deletion of userData secret failed")
				return err
			}
			if err := r.deleteSecret(log, rclient, remoteHost, "meta", remoteHost.Spec.MetaData); err != nil {
				log.Error(err, "deletion of metaData secret failed")
				return err
			}
			if err := r.deleteSecret(log, rclient, remoteHost, "network", remoteHost.Spec.NetworkData); err != nil {
				log.Error(err, "deletion of networkData secret failed")
				return err
			}
			if err := rclient.Delete(remoteHostName); err != nil {
				log.Error(err, "deletion of remote host failed")
				return err
			}
			remoteHost, err = rclient.Get(remoteHostName)
			if err != nil {
				log.Error(err, "check of remote host existence failed")
				return err
			}
			stillExists = (remoteHost != nil)
			return nil
		}()
		if err != nil {
			log.V(1).Info("something failed during deletion")
			return ctrl.Result{}, err
		}
		if stillExists {
			return ctrl.Result{RequeueAfter: time.Second * 10}, nil
		}
		controllerutil.RemoveFinalizer(host, hostv1alpha1.HostFinalizer)
		err = r.Update(ctx, host)
		if err != nil {
			log.Error(err, "removal of finalizer failed")
			return ctrl.Result{}, err
		}
	}
	return ctrl.Result{}, nil
}

// Retropropagate maps from remote target to source
//
// Transfer elements from argument targetMap to sourceMap and deletes the elements from
// sourceMap whose key is in synced and do not appear in targetMap. The result of the
// function is the comma separated names of keys from targetMap.
// domains ending in kubernetes.io are protected (no sync) and domain host.kubernetes.io
// is propagated from sourceMap to targetMap
func syncMaps(sourceMap, targetMap map[string]string, synced string) string {
	var keys []string
	if synced != "" {
		keys = strings.Split(synced, ",")
	}
	for _, key := range keys {
		_, ok := targetMap[key]
		if !ok {
			delete(sourceMap, key)
		}
	}
	keys = nil
	for key, v := range targetMap {
		elts := strings.Split(key, "/")
		if len(elts) == 2 {
			domain := elts[0]
			if domain == "kubernetes.io" || strings.HasSuffix(domain, ".kubernetes.io") {
				continue
			}
			// clean host domain. It is populated later with current values.
			if domain == hostv1alpha1.HostDomain {
				delete(targetMap, key)
				continue
			}
		}
		keys = append(keys, key)
		sourceMap[key] = v
	}
	for key, v := range sourceMap {
		elts := strings.Split(key, "/")
		if len(elts) != 2 || elts[0] != hostv1alpha1.HostDomain {
			continue
		}
		// Exclude the synchronization annotations: they are present with different
		// values at both levels.
		if key == hostv1alpha1.SyncedAnnotationsAnnotation || key == hostv1alpha1.SyncedLabelsAnnotation {
			continue
		}
		targetMap[key] = v
	}
	return strings.Join(keys, ",")
}

func (r *HostReconciler) deleteSecret(log logr.Logger, rclient *RemoteClient, host *hostv1alpha1.Host, role string, ref *corev1.SecretReference) error {
	if ref == nil || host.Annotations == nil {
		return nil
	}
	syncAnnotationName := RemoteDomain + "/" + role
	if _, ok := host.Annotations[syncAnnotationName]; !ok {
		return nil
	}
	log.V(1).Info("Deleting secret", "role", role)
	return rclient.DeleteSecret(host.Name, role)
}

func (r *HostReconciler) synchronizeSecret(ctx context.Context, log logr.Logger, rclient *RemoteClient, host *hostv1alpha1.Host, role string, ref *corev1.SecretReference) error {
	log = log.WithValues("secretRole", role)
	if ref == nil {
		log.V(1).Info("no secret with that role")
		return nil
	}
	var secret corev1.Secret
	namespace := ref.Namespace
	if namespace == "" {
		namespace = host.Namespace
	}
	log = log.WithValues("secretName", ref.Name, "secretNamespace", namespace)
	key := client.ObjectKey{Name: ref.Name, Namespace: namespace}
	err := r.Get(ctx, key, &secret)
	if err != nil {
		log.Error(err, "cannot get secret", "secretName", ref.Name, "secretNamespace", namespace)
		return err
	}
	remoteHostName := fmt.Sprintf("remote-%s", host.Name)
	syncAnnotationName := RemoteDomain + "/" + role
	var syncedGeneration int64
	if host.Annotations == nil {
		host.Annotations = map[string]string{}
	}
	syncedValue, ok := host.Annotations[syncAnnotationName]
	if ok {
		syncedGeneration, err = strconv.ParseInt(syncedValue, 10, 64)
		if err != nil {
			log.Error(err, "cannot parse synced generation")
			return err
		}
		if secret.Generation != syncedGeneration {
			log.V(1).Info("update secret")
			err := rclient.CreateUpdateSecret(remoteHostName, role, &secret)
			if err != nil {
				return err
			}
		}
	} else {
		log.V(1).Info("create secret")
		err := rclient.CreateUpdateSecret(remoteHostName, role, &secret)
		if err != nil {
			return err
		}
	}
	host.Annotations[syncAnnotationName] = fmt.Sprint(secret.Generation)
	return nil
}

// SetupWithManager sets up the controller with the Manager.
func (r *HostReconciler) SetupWithManager(mgr ctrl.Manager) error {
	channel := make(chan event.GenericEvent)
	r.channel = channel
	src := source.Channel{Source: channel}
	return ctrl.NewControllerManagedBy(mgr).
		// Uncomment the following line adding a pointer to an instance of the controlled resource as an argument
		For(&hostv1alpha1.Host{}).
		WatchesRawSource(&src, &handler.EnqueueRequestForObject{}).
		Complete(r)
}
