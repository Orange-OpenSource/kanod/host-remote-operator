package controller

import (
	"context"
	"encoding/json"
	"net/http"
	"strings"
	"sync"
	"time"

	hostv1alpha1 "gitlab.com/Orange-OpenSource/kanod/host-operator/api/v1alpha1"
	"golang.org/x/crypto/bcrypt"
	"golang.org/x/exp/maps"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"

	logr "github.com/go-logr/logr"
	"github.com/gorilla/mux"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
)

type RemoteHostServer struct {
	Client   client.Client
	Watchers map[string][]chan<- string
	Auth     map[string][]byte
	Log      logr.Logger
	mutex    sync.Mutex
}

func (rhs *RemoteHostServer) SendWatch(ctx context.Context, obj client.Object) []reconcile.Request {
	annotations := obj.GetAnnotations()
	if annotations == nil {
		return nil
	}
	key, ok1 := annotations[RemoteKeyAnnotation]
	namespace := obj.GetNamespace()
	writers, ok2 := rhs.Watchers[namespace]
	if !ok1 || !ok2 {
		return nil
	}
	rhs.Log.V(1).Info("send watch event", "namespace", namespace, "key", key)
	for _, ch := range writers {
		ch <- key
	}

	return nil
}

func (rhs *RemoteHostServer) AddUser(username, password string) error {
	hash, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		return err
	}
	rhs.Auth[username] = hash
	return nil
}

func (rhs *RemoteHostServer) AddUserHash(username string, hash []byte) error {
	rhs.Auth[username] = hash
	return nil
}

func (rhs *RemoteHostServer) verifyUserPass(username, password string) bool {
	wantPass, hasUser := rhs.Auth[username]
	if !hasUser {
		return false
	}
	if cmperr := bcrypt.CompareHashAndPassword(wantPass, []byte(password)); cmperr == nil {
		return true
	}
	return false
}

func (rhs *RemoteHostServer) createHost(w http.ResponseWriter, r *http.Request) {
	namespace, pass, ok := r.BasicAuth()
	if !ok || !rhs.verifyUserPass(namespace, pass) {
		w.Header().Set("WWW-Authenticate", `Basic realm="hosts"`)
		http.Error(w, "Unauthorized", http.StatusUnauthorized)
		return
	}
	var host hostv1alpha1.Host
	ctx := r.Context()
	err := json.NewDecoder(r.Body).Decode(&(host.Spec))
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	vars := mux.Vars(r)
	host.Name = vars["name"]
	host.Namespace = namespace
	fixSecret(&host)
	rhs.Log.V(1).Info("server create host", "name", host.Name, "namespace", host.Namespace)
	err = rhs.Client.Create(ctx, &host)
	if err != nil {
		rhs.Log.V(1).Error(err, "error in createHost")
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

}

func (rhs *RemoteHostServer) deleteHost(w http.ResponseWriter, r *http.Request) {
	namespace, pass, ok := r.BasicAuth()
	if !ok || !rhs.verifyUserPass(namespace, pass) {
		w.Header().Set("WWW-Authenticate", `Basic realm="hosts"`)
		http.Error(w, "Unauthorized", http.StatusUnauthorized)
		return
	}
	var host hostv1alpha1.Host
	ctx := r.Context()
	vars := mux.Vars(r)
	key := types.NamespacedName{
		Name:      vars["name"],
		Namespace: namespace,
	}
	err := rhs.Client.Get(ctx, key, &host)
	if err != nil {
		// Already deleted no reason to fail
		if errors.IsNotFound(err) {
			w.Header().Set("Content-Type", "application/json")
			json.NewEncoder(w).Encode(map[string]string{})
		} else {
			http.Error(w, err.Error(), http.StatusBadRequest)
		}
		return
	}
	rhs.Log.V(1).Info("server delete host", "name", host.Name)
	err = rhs.Client.Delete(ctx, &host)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(map[string]string{})
}

func (rhs *RemoteHostServer) watchHosts(w http.ResponseWriter, r *http.Request) {
	namespace, pass, ok := r.BasicAuth()
	if !ok || !rhs.verifyUserPass(namespace, pass) {
		w.Header().Set("WWW-Authenticate", `Basic realm="hosts"`)
		http.Error(w, "Unauthorized", http.StatusUnauthorized)
		return
	}
	log := rhs.Log.WithValues("namespace", namespace)
	log.V(1).Info("establish watch connection")
	flusher, ok := w.(http.Flusher)
	if !ok {
		http.Error(w, "Flush not supported", http.StatusMethodNotAllowed)
		return
	}
	w.Header().Set("Connection", "Keep-Alive")
	w.Header().Set("Transfer-Encoding", "chunked")
	w.Header().Set("X-Content-Type-Options", "nosniff")
	w.WriteHeader(http.StatusOK)
	notifications := make(chan string, 10)
	rhs.mutex.Lock()
	rhs.Watchers[namespace] = append(rhs.Watchers[namespace], notifications)
	rhs.mutex.Unlock()
	ctx := r.Context()
loop:
	for {
		select {
		case message, ok := <-notifications:
			// With a single channel to watch for close this should work.
			if !ok {
				break loop
			}
			log.V(1).Info("notifying", "message", message)
			w.Write([]byte(message))
			w.Write([]byte{'\n'})
			flusher.Flush()
		case <-time.After(20 * time.Second):
			w.Write([]byte{'*', '\n'})
			flusher.Flush()
		case <-ctx.Done():
			break loop
		}
	}
}

func (rhs *RemoteHostServer) getHost(w http.ResponseWriter, r *http.Request) {
	namespace, pass, ok := r.BasicAuth()
	if !ok || !rhs.verifyUserPass(namespace, pass) {
		w.Header().Set("WWW-Authenticate", `Basic realm="hosts"`)
		http.Error(w, "Unauthorized", http.StatusUnauthorized)
		return
	}
	var host hostv1alpha1.Host
	ctx := r.Context()
	vars := mux.Vars(r)
	key := types.NamespacedName{
		Name:      vars["name"],
		Namespace: namespace,
	}
	rhs.Log.V(1).Info("server get host", "name", vars["name"])
	err := rhs.Client.Get(ctx, key, &host)
	if err != nil {
		if errors.IsNotFound(err) {
			http.Error(w, err.Error(), http.StatusNotFound)
		} else {
			http.Error(w, err.Error(), http.StatusBadRequest)
		}
		return
	}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(host)
}

func (rhs *RemoteHostServer) updateHost(w http.ResponseWriter, r *http.Request) {
	namespace, pass, ok := r.BasicAuth()
	if !ok || !rhs.verifyUserPass(namespace, pass) {
		w.Header().Set("WWW-Authenticate", `Basic realm="hosts"`)
		http.Error(w, "Unauthorized", http.StatusUnauthorized)
		return
	}
	var host hostv1alpha1.Host
	ctx := r.Context()
	vars := mux.Vars(r)
	key := types.NamespacedName{
		Name:      vars["name"],
		Namespace: namespace,
	}
	log := rhs.Log.WithValues("namespace", namespace, "name", vars["name"])
	err := rhs.Client.Get(ctx, key, &host)
	if err != nil {
		if errors.IsNotFound(err) {
			http.Error(w, err.Error(), http.StatusNotFound)
		} else {
			http.Error(w, err.Error(), http.StatusBadRequest)
		}
		return
	}
	w.Header().Set("Content-Type", "application/json")
	var newHost hostv1alpha1.Host

	err = json.NewDecoder(r.Body).Decode(&(newHost))
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	host.Spec = newHost.Spec
	host.Labels = updateMaps(log.WithValues("map", "labels"), host.Labels, newHost.Labels)
	host.Annotations = updateMaps(log.WithValues("map", "annotations"), host.Annotations, newHost.Annotations)
	fixSecret(&host)
	rhs.Log.V(1).Info("server update host", "name", host.Name)
	err = rhs.Client.Update(ctx, &host)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(map[string]string{})
}

// UpdateMaps pushes labels/annotations belonging to host down to the remote host
//
// It keeps the other annotations intact.
func updateMaps(log logr.Logger, target, source map[string]string) map[string]string {

	log.V(1).Info("update keys in maps",
		"source", strings.Join(maps.Keys(source), ","),
		"target", strings.Join(maps.Keys(target), ","))
	if target == nil {
		target = map[string]string{}
	}
	for key := range target {
		elts := strings.Split(key, "/")
		if len(elts) == 2 {
			domain := elts[0]
			if domain == hostv1alpha1.HostDomain || strings.HasSuffix(domain, "."+hostv1alpha1.HostDomain) {
				delete(target, key)
			}
		}
	}
	for key, v := range source {
		elts := strings.Split(key, "/")
		if len(elts) == 2 {
			domain := elts[0]
			if domain == hostv1alpha1.HostDomain || strings.HasSuffix(domain, "."+hostv1alpha1.HostDomain) {
				target[key] = v
			}
		}
	}
	log.V(1).Info("new map", "keys", strings.Join(maps.Keys(target), ","))
	return target
}

// Retarget a secret reference
//
// The predictable names of secrets are derived from the host names and the role they
// have. Only three roles are recognized.
func fixSecretRef(host *hostv1alpha1.Host, role string, ref *corev1.SecretReference) {
	if ref == nil {
		return
	}
	ref.Name = host.Name + "-" + role
	ref.Namespace = host.Namespace
}

// Retarget host secrets to local names
//
// When secrets are transfered they are renamed locally with predictable names
// We ensure we use those targets. As the security of this hosting cluster is
// at stake, this operation must be done on this cluster.
func fixSecret(host *hostv1alpha1.Host) {
	fixSecretRef(host, "user", host.Spec.UserData)
	fixSecretRef(host, "network", host.Spec.NetworkData)
	fixSecretRef(host, "meta", host.Spec.MetaData)
}

func (rhs *RemoteHostServer) deleteSecret(w http.ResponseWriter, r *http.Request) {
	namespace, pass, ok := r.BasicAuth()
	if !ok || !rhs.verifyUserPass(namespace, pass) {
		w.Header().Set("WWW-Authenticate", `Basic realm="hosts"`)
		http.Error(w, "Unauthorized", http.StatusUnauthorized)
		return
	}
	var secret corev1.Secret
	ctx := r.Context()
	vars := mux.Vars(r)
	role := vars["role"]
	hostName := vars["name"]
	switch role {
	case "user", "network", "meta":
		break
	default:
		http.Error(w, "Bad role", http.StatusBadRequest)
		return
	}
	name := hostName + "-" + role
	key := types.NamespacedName{
		Name:      name,
		Namespace: namespace,
	}
	err := rhs.Client.Get(ctx, key, &secret)
	if err != nil {
		if errors.IsNotFound(err) {
			w.Header().Set("Content-Type", "application/json")
			json.NewEncoder(w).Encode(map[string]string{})
		} else {
			http.Error(w, err.Error(), http.StatusBadRequest)
		}
		return
	}
	rhs.Log.V(1).Info("server delete secret", "name", secret.Name)
	err = rhs.Client.Delete(ctx, &secret)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(map[string]string{})
}

func (rhs *RemoteHostServer) createUpdateSecret(w http.ResponseWriter, r *http.Request) {
	log := rhs.Log.WithValues("action", "createUpdateSecret")
	namespace, pass, ok := r.BasicAuth()
	if !ok || !rhs.verifyUserPass(namespace, pass) {
		log.V(1).Info("authentication failed", "namespace", namespace)
		w.Header().Set("WWW-Authenticate", `Basic realm="hosts"`)
		http.Error(w, "Unauthorized", http.StatusUnauthorized)
		return
	}
	log = log.WithValues("namespace", namespace)
	var newSecret corev1.Secret
	err := json.NewDecoder(r.Body).Decode(&(newSecret))
	if err != nil {
		log.Error(err, "cannot decode secret")
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	var secret corev1.Secret
	ctx := r.Context()
	vars := mux.Vars(r)
	role := vars["role"]
	hostName := vars["name"]
	log = log.WithValues("hostName", hostName, "role", role)
	switch role {
	case "user", "network", "meta":
		break
	default:
		http.Error(w, "Bad role", http.StatusBadRequest)
		return
	}
	name := hostName + "-" + role
	key := types.NamespacedName{
		Name:      name,
		Namespace: namespace,
	}

	err = rhs.Client.Get(ctx, key, &secret)
	if err != nil {
		if errors.IsNotFound(err) {
			secret.ObjectMeta = v1.ObjectMeta{
				Name:      name,
				Namespace: namespace,
			}
			secret.Data = newSecret.Data
			secret.Type = corev1.SecretTypeOpaque // Safer to force it
			rhs.Log.V(1).Info("server create secret", "name", secret.Name)
			err = rhs.Client.Create(ctx, &secret)
			if err != nil {
				log.Error(err, "Create Secret failed", "secret", secret)
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
		} else {
			http.Error(w, err.Error(), http.StatusBadRequest)
		}
		return
	}
	w.Header().Set("Content-Type", "application/json")
	secret.Data = newSecret.Data
	err = rhs.Client.Update(ctx, &secret)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(map[string]string{})
}

func (rhs *RemoteHostServer) Serve() {
	router := mux.NewRouter()
	router.HandleFunc("/hosts/{name}", rhs.getHost).Methods(http.MethodGet)
	router.HandleFunc("/hosts/{name}", rhs.createHost).Methods(http.MethodPost)
	router.HandleFunc("/hosts/{name}", rhs.updateHost).Methods(http.MethodPut)
	router.HandleFunc("/hosts/{name}", rhs.deleteHost).Methods(http.MethodDelete)
	router.HandleFunc("/hosts/", rhs.watchHosts).Methods("WATCH")
	router.HandleFunc("/secrets/{name}/{role}", rhs.createUpdateSecret).Methods(http.MethodPost)
	router.HandleFunc("/secrets/{name}/{role}", rhs.deleteSecret).Methods(http.MethodDelete)
	http.ListenAndServe(":8000", router)
}
