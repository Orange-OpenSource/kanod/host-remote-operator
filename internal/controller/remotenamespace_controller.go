/*
Copyright 2024 Orange.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controller

import (
	"context"
	"fmt"
	"time"

	"github.com/google/uuid"
	hostv1alpha1 "gitlab.com/Orange-OpenSource/kanod/host-operator/api/v1alpha1"
	remotev1alpha1 "gitlab.com/Orange-OpenSource/kanod/host-remote-operator/api/v1alpha1"
	"golang.org/x/crypto/bcrypt"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	capiv1beta1 "sigs.k8s.io/cluster-api/api/v1beta1"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	logr "sigs.k8s.io/controller-runtime/pkg/log"
)

// RemoteNamespaceReconciler reconciles a RemoteNamespace object
type RemoteNamespaceReconciler struct {
	client.Client
	Scheme *runtime.Scheme
	Server *RemoteHostServer
}

const (
	HashPasswordAnnotation = "host.kanod.io/hashed-password"
	RemoteSecretType       = "host.kanod.io/remote-config"
	UrlKey                 = "URL"
	PasswordKey            = "password"
	NamespaceKey           = "namespace"
	CaKey                  = "ca.crt"
)

//+kubebuilder:rbac:groups=kanod.io,resources=remotenamespaces,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=kanod.io,resources=remotenamespaces/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=kanod.io,resources=remotenamespaces/finalizers,verbs=update
//+kubebuilder:rbac:groups="",resources=secrets,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups="cluster.x-k8s.io",resources=clusters,verbs=get;list

// Reconcile is part of the main kubernetes reconciliation loop which aims to
// move the current state of the cluster closer to the desired state.
// TODO(user): Modify the Reconcile function to compare the state specified by
// the RemoteNamespace object against the actual cluster state, and then
// perform operations to make the cluster state reflect the state specified by
// the user.
//
// For more details, check Reconcile and its Result here:
// - https://pkg.go.dev/sigs.k8s.io/controller-runtime@v0.15.0/pkg/reconcile
func (r *RemoteNamespaceReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	log := logr.FromContext(ctx)
	var rns remotev1alpha1.RemoteNamespace
	log.V(1).Info("reconciliation")
	err := r.Get(ctx, req.NamespacedName, &rns)
	if err != nil {
		if errors.IsNotFound(err) {
			log.V(1).Info("not found")
			return ctrl.Result{}, nil
		}
		log.Error(err, "Cannot retrieve Remote Namespace")
		return ctrl.Result{}, err
	}
	if rns.Annotations == nil {
		rns.Annotations = map[string]string{}
	}
	passwordHash, ok := rns.Annotations[HashPasswordAnnotation]

	if ok {
		log.Info("Remote namespace already configured")
		r.Server.AddUserHash(rns.Namespace, []byte(passwordHash))
		return ctrl.Result{}, err
	}

	log.V(1).Info("launching secret creation")
	secretNs := rns.Spec.Secret.Namespace
	if secretNs == "" {
		secretNs = rns.Namespace
	}
	var secret = corev1.Secret{
		ObjectMeta: metav1.ObjectMeta{Name: rns.Spec.Secret.Name, Namespace: secretNs},
	}
	var cluster capiv1beta1.Cluster
	if rns.Spec.Cluster != "" {
		key := client.ObjectKey{Namespace: secretNs, Name: rns.Spec.Cluster}
		err := r.Get(ctx, key, &cluster)
		if err != nil {
			if errors.IsNotFound(err) {
				log.V(1).Info("Waiting for owning cluster")
				return ctrl.Result{RequeueAfter: time.Second * 10}, nil
			} else {
				log.Error(err, "problem accessing owning cluster")
				return ctrl.Result{}, err
			}
		}
	}

	var pwd []byte
	_, err = controllerutil.CreateOrUpdate(ctx, r.Client, &secret, func() error {
		if secret.Type == "" {
			secret.Type = RemoteSecretType
		} else if secret.Type != RemoteSecretType {
			return fmt.Errorf("not a remote config secret: %s", secret.Type)
		}
		if secret.Data == nil {
			secret.Data = map[string][]byte{}
		}
		secret.Data[UrlKey] = []byte(rns.Spec.URL)
		if pwd, ok = secret.Data[PasswordKey]; !ok {
			pwd = []byte(uuid.New().String())
			secret.Data[PasswordKey] = []byte(pwd)
		}
		secret.Data[NamespaceKey] = []byte(rns.Namespace)
		if rns.Spec.Certificate != "" {
			secret.Data[CaKey] = []byte(rns.Spec.Certificate)
		}
		if rns.Spec.Cluster != "" {
			if err := controllerutil.SetOwnerReference(&cluster, &secret, r.Scheme); err != nil {
				return err
			}
		}
		return nil
	})
	if err != nil {
		log.Error(err, "cannot create/update associated secret")
		return ctrl.Result{}, err
	}
	passwordHashBytes, err := bcrypt.GenerateFromPassword(pwd, bcrypt.DefaultCost)
	if err != nil {
		log.Error(err, "cannot compute password hash")
		return ctrl.Result{}, err
	}
	log.V(1).Info("update annotation with password")
	rns.Annotations[HashPasswordAnnotation] = string(passwordHashBytes)
	err = r.Update(ctx, &rns)
	if err != nil {
		log.Error(err, "cannot update RemoteNamespace with password hash")
		return ctrl.Result{}, err
	}

	return ctrl.Result{}, nil
}

// SetupWithManager sets up the controller with the Manager.
func (r *RemoteNamespaceReconciler) SetupWithManager(mgr ctrl.Manager) error {
	r.Server = &RemoteHostServer{
		Client:   r.Client,
		Watchers: map[string][]chan<- string{},
		Auth:     map[string][]byte{},
		Log:      mgr.GetLogger().WithName("remote-host-server"),
	}
	go r.Server.Serve()
	return ctrl.NewControllerManagedBy(mgr).
		For(&remotev1alpha1.RemoteNamespace{}).
		Watches(
			&hostv1alpha1.Host{},
			handler.EnqueueRequestsFromMapFunc(r.Server.SendWatch)).
		Complete(r)
}
