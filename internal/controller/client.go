package controller

import (
	"bufio"
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strings"

	hostv1alpha1 "gitlab.com/Orange-OpenSource/kanod/host-operator/api/v1alpha1"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/event"
	logr "sigs.k8s.io/controller-runtime/pkg/log"
)

type RemoteClient struct {
	URL       string
	Namespace string
	Password  string
	Client    http.Client
}

var globalLog = logr.Log.WithName("remote-client")

func (r *RemoteClient) Get(name string) (*hostv1alpha1.Host, error) {
	log := globalLog.WithValues("host", name, "namespace", r.Namespace)
	var host hostv1alpha1.Host
	url := r.URL + "/hosts/" + name
	log.V(1).Info("client get", "url", url)
	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		log.Error(err, "get: bad request")
		return nil, err
	}
	req.SetBasicAuth(r.Namespace, r.Password)
	resp, err := r.Client.Do(req)
	if err != nil {
		log.Error(err, "get: request failed")
		return nil, err
	}
	defer resp.Body.Close()
	switch resp.StatusCode {
	case http.StatusOK:
		break
	case http.StatusNotFound:
		return nil, nil
	default:
		log.V(1).Info("bad status", "status", resp.StatusCode)
		return nil, fmt.Errorf("bad status: %d", resp.StatusCode)
	}
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Error(err, "get: cannot read body")
		return nil, err
	}
	json.Unmarshal(body, &host)
	return &host, nil
}

func (r *RemoteClient) Delete(name string) error {
	log := globalLog.WithValues("host", name, "namespace", r.Namespace)
	url := r.URL + "/hosts/" + name
	log.V(1).Info("client delete", "url", url)
	req, err := http.NewRequest(http.MethodDelete, url, nil)
	if err != nil {
		log.V(1).Info("delete: bad request")
		return err
	}
	req.SetBasicAuth(r.Namespace, r.Password)
	resp, err := r.Client.Do(req)
	if err != nil {
		log.V(1).Info("delete: request failed")
		return err
	}
	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("bad status: %d", resp.StatusCode)
	}
	log.V(1).Info("client delete performed")
	return nil
}

func (r *RemoteClient) Create(name string, host *hostv1alpha1.Host) error {
	log := globalLog.WithValues("host", name, "namespace", r.Namespace)
	url := r.URL + "/hosts/" + name
	log.V(1).Info("client create", "url", url)
	jsonData, err := json.Marshal(host.Spec)
	if err != nil {
		log.Error(err, "create: marshal failed")
		return err
	}
	req, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(jsonData))
	if err != nil {
		log.Error(err, "create: bad request")
		return err
	}
	req.SetBasicAuth(r.Namespace, r.Password)
	resp, err := r.Client.Do(req)
	if err != nil {
		log.Error(err, "request failed")
		return err
	}
	if resp.StatusCode != http.StatusOK {
		err := fmt.Errorf("bad status: %d", resp.StatusCode)
		log.Error(err, "bad status")
		return err
	}
	return nil
}

func (r *RemoteClient) Update(name string, host *hostv1alpha1.Host) error {
	log := globalLog.WithValues("host", name, "namespace", r.Namespace)
	url := r.URL + "/hosts/" + name
	log.V(1).Info("client update", "url", url)
	jsonData, err := json.Marshal(host)
	if err != nil {
		log.Error(err, "update: marshal failed")
		return err
	}
	req, err := http.NewRequest(http.MethodPut, url, bytes.NewBuffer(jsonData))
	if err != nil {
		log.Error(err, "update: bad request")
		return err
	}
	req.SetBasicAuth(r.Namespace, r.Password)
	resp, err := r.Client.Do(req)
	if err != nil {
		log.Error(err, "update: request failed")
		return err
	}
	if resp.StatusCode != http.StatusOK {
		err := fmt.Errorf("bad status: %d", resp.StatusCode)
		log.Error(err, "update: status")
		return err
	}
	return nil
}

func (r *RemoteClient) Watch(ch chan event.GenericEvent) error {
	log := globalLog.WithValues("namespace", r.Namespace)
	log.V(1).Info("watch")
	req, err := http.NewRequest("WATCH", r.URL+"/hosts/", nil)
	if err != nil {
		log.Error(err, "watch: bad request")
		return err
	}
	req.SetBasicAuth(r.Namespace, r.Password)
	resp, err := r.Client.Do(req)
	if err != nil {
		log.Error(err, "watch: request failed")
		return err
	}
	if resp.StatusCode != http.StatusOK {
		err := fmt.Errorf("bad status: %d", resp.StatusCode)
		log.Error(err, "watch: status")
		return err
	}
	log.V(1).Info("watch client side", "namespace", r.Namespace)
	defer resp.Body.Close()
	scanner := bufio.NewScanner(resp.Body)
	for scanner.Scan() {
		log.V(1).Info("read attempt")
		key := scanner.Text()
		if key == "*" {
			log.V(1).Info("discard keep alive")
			continue
		}
		elt := strings.Split(key, "/")
		if len(elt) == 2 {
			namespace := elt[0]
			name := elt[1]
			obj := metav1.PartialObjectMetadata{
				ObjectMeta: metav1.ObjectMeta{
					Namespace: namespace,
					Name:      name,
				},
			}
			log.V(1).Info("notify", "host", name, "namespace", namespace)
			ch <- event.GenericEvent{Object: &obj}
		} else {
			log.V(1).Info("watch: cannot parse key", "key", key)
		}
	}
	return nil
}

func (r *RemoteClient) CreateUpdateSecret(name string, role string, secret *corev1.Secret) error {
	log := globalLog.WithValues("host", name, "secretRole", role, "namespace", r.Namespace)
	url := r.URL + "/secrets/" + name + "/" + role
	log.V(1).Info("client secret create/update", "url", url)
	jsonData, err := json.Marshal(secret)
	if err != nil {
		log.Error(err, "create secret: marshal failed")
		return err
	}
	req, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(jsonData))
	if err != nil {
		log.Error(err, "create secret: bad request")
		return err
	}
	req.SetBasicAuth(r.Namespace, r.Password)
	resp, err := r.Client.Do(req)
	if err != nil {
		log.Error(err, "create secret: request failed")
		return err
	}
	if resp.StatusCode != http.StatusOK {
		err := fmt.Errorf("bad status: %d", resp.StatusCode)
		log.Error(err, "create secret bad status")
		return err
	}
	return nil
}

func (r *RemoteClient) DeleteSecret(name string, role string) error {
	log := globalLog.WithValues("host", name, "secretRole", role, "namespace", r.Namespace)
	url := r.URL + "/secrets/" + name + "/" + role
	log.V(1).Info("client secret delete", "url", url)
	req, err := http.NewRequest(http.MethodDelete, url, nil)
	if err != nil {
		log.V(1).Info("delete secret: bad request")
		return err
	}
	req.SetBasicAuth(r.Namespace, r.Password)
	resp, err := r.Client.Do(req)
	if err != nil {
		log.V(1).Info("delete secret: request failed")
		return err
	}
	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("delete secret bad status: %d", resp.StatusCode)
	}
	return nil
}
